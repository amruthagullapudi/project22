import tkinter as tk
from tkinter import ttk

def open_search_recipe():
    # Function to open the "Search for Recipe" page
    new_window = tk.Toplevel(root)
    new_window.title("enter your Recipe")

def open_enter_recipe():
    # Function to open the "Enter Your Recipe" page
    # You can replace this function with the code for the "Enter Your Recipe" page
    new_window = tk.Toplevel(root)
    new_window.title("search for Recipe")
    new_window.geometry("300x200")
    label = tk.Label(new_window, text="You have selected: search for Recipe")
    label.pack()
    
    def display_menu():
        # Function to display the menu based on the selected recipe type
        selected_recipe = var.get()
        if selected_recipe == "Veg":
            menu_label.config(text="Veg Menu:")
            # Replace this part with the actual veg menu items you want to display
            menu_items = ["Veg Dish 1", "Veg Dish 2", "Veg Dish 3"]
        else:
            menu_label.config(text="Non-Veg Menu:")
            # Replace this part with the actual non-veg menu items you want to display
            menu_items = ["Non-Veg Dish 1", "Non-Veg Dish 2", "Non-Veg Dish 3"]

        menu_listbox.delete(0, tk.END)
        for item in menu_items:
            menu_listbox.insert(tk.END, item)

    # Create a label to prompt the user to select a recipe type
    label = tk.Label(new_window, text="Select Recipe Type:")
    label.pack(pady=10)

    # Create a variable to hold the selected recipe type
    var = tk.StringVar(new_window)

    # Create radio buttons for veg and non-veg options
    tk.Radiobutton(new_window, text="Veg", variable=var, value="Veg").pack(anchor=tk.W)
    tk.Radiobutton(new_window, text="Non-Veg", variable=var, value="Non-Veg").pack(anchor=tk.W)
# Create a button to display the menu based on the selected recipe type
    display_button = tk.Button(new_window, text="Display Menu", command=display_menu)
    display_button.pack(pady=10)

    # Create a label to display the menu
    menu_label = tk.Label(new_window, text="")
    menu_label.pack()

    # Create a listbox to show the menu items
    menu_listbox = tk.Listbox(new_window)
    menu_listbox.pack()

root = tk.Tk()
root.title("Recipes")

# Add the heading "WORLD OF RECIPES" before the notebook
heading_label = tk.Label(root, text="WORLD OF RECIPES", font=("Arial", 20))
heading_label.pack(pady=10)

# Create a notebook (tab) widget
notebook = ttk.Notebook(root)

# Create two frames for the two options
frame1 = tk.Frame(notebook)
frame2 = tk.Frame(notebook)

# Add the frames to the notebook with respective tab names
notebook.add(frame1, text="Search for Recipe")
notebook.add(frame2, text="Enter your Recipe")

notebook.pack(pady=10)

# Add content to the frames
# Option 1 content
option1_label = tk.Label(frame1, text="Welcome to search for recipe")
option1_label.pack(pady=20)
option1_button = tk.Button(frame1, text="Select search for Recipe", command=open_enter_recipe)
option1_button.pack()
# Option 2 content
option2_label = tk.Label(frame2, text="Welcome to enter your Recipe")
option2_label.pack(pady=20)
option2_button = tk.Button(frame2, text="Select enter your Recipe", command=open_search_recipe)
option2_button.pack()

root.mainloop()
